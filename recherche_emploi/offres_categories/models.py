from datetime import datetime
from sqlalchemy import Column, Integer, String, Float, ForeignKey, Text, DateTime
from sqlalchemy.orm import relationship

from recherche_emploi.database.db import Base


class Categorie(Base):
    __tablename__ = "Categories"

    id = Column(Integer, primary_key=True, autoincrement=True)
    nom = Column(String(50))
    description = Column(Text)
    offre = relationship("Offre", back_populates="categorie")


class Offre(Base):
    __tablename__ = "Offres"

    id = Column(Integer, primary_key=True, autoincrement=True)
    date_publication = Column(DateTime, default=datetime.now)
    date_expiration = Column(DateTime, default=datetime.now)
    intitule = Column(String(50))
    description = Column(Text)
    salaire_min = Column(Integer)
    salaire_max = Column(Integer)
    mot_cle = Column(String)
    categorie_id = Column(Integer, ForeignKey('Categorie.id', ondelete="CASCADE"), )
    categorie = relationship("Categorie", back_populates="offre")
    entreprise = relationship("Entreprise", back_populates="offre")