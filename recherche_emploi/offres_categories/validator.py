from typing import Optional

from sqlalchemy.orm import Session

from .models import Categorie


async def verify_categorie_exist(categorie_id: int, db_session: Session) -> Optional[Categorie]:
    return db_session.query(Categorie).filter(Categorie.id == categorie_id).first()