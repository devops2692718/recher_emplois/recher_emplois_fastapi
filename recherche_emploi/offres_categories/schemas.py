from datetime import datetime
from typing import Optional

from pydantic import BaseModel, constr


class Categorie(BaseModel):
    nom: str
    description = str


class ListCategorie(BaseModel):
    id: int
    nom: str
    description = str

    class Config:
        orm_mode = True


class OffreBase(BaseModel):
    id: Optional[int]
    date_publication = datetime
    date_expiration = datetime
    intitule = str
    description = str
    salaire_min = int
    salaire_max = int

    class Config:
        orm_mode = True


class Offre(OffreBase):
    category_id: int


class OffreListing(OffreBase):
    category: ListCategorie

    class Config:
        orm_mode = True