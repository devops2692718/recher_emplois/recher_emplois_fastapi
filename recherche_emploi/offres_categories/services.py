from typing import List

from fastapi import HTTPException, status

from . import models


async def create_new_categorie(request, database) -> models.Categorie:
    new_categorie = models.Categorie(name=request.name)
    database.add(new_categorie)
    database.commit()
    database.refresh(new_categorie)
    return new_categorie


async def get_all_categories(database) -> List[models.Categorie]:
    categories = database.query(models.Categorie).all()
    return categories


async def get_categorie_by_id(category_id, database) -> models.Categorie:
    categorie_info = database.query(models.Categorie).get(category_id)
    if not categorie_info:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Data Not Found !")
    return categorie_info


async def delete_categorie_by_id(category_id, database):
    database.query(models.Categorie).filter(models.Categorie.id == category_id).delete()
    database.commit()


async def create_new_offre(request, database) -> models.Offre:
    new_offre = models.Product(date_publication=request.date_publication, date_expiration=request.date_expiration,
                                 intitule=request.intitule,
                                 description=request.description, salaire_min=request.salaire_min,
                                 salaire_max=request.salaire_max, mot_cle=request.mot_cle)
                                 
    database.add(new_offre)
    database.commit()
    database.refresh(new_offre)
    return new_offre


async def get_all_offres(database) -> List[models.Offre]:
    offre = database.query(models.Offre).all()
    return offre