from typing import List

from fastapi import APIRouter, Depends, status, Response, HTTPException
from sqlalchemy.orm import Session

from recherche_emploi.database import db
from . import schemas
from . import services
from . import validator

router = APIRouter(
    tags=['Offres'],
    prefix='/offres'
)


@router.post('/categorie', status_code=status.HTTP_201_CREATED)
async def create_category(request: schemas.Categorie, database: Session = Depends(db.get_db)):
    new_category = await services.create_new_category(request, database)
    return new_category


@router.get('/categorie', response_model=List[schemas.ListCategorie])
async def get_all_categories(database: Session = Depends(db.get_db)):
    return await services.get_all_categories(database)


@router.get('/categorie/{category_id}', response_model=schemas.ListCategorie)
async def get_categorie_by_id(categorie_id: int, database: Session = Depends(db.get_db)):
    return await services.get_categorie_by_id(categorie_id, database)


@router.delete('/categorie/{categorie_id}', status_code=status.HTTP_204_NO_CONTENT, response_class=Response)
async def delete_categorie_by_id(categorie_id: int, database: Session = Depends(db.get_db)):
    return await services.delete_categorie_by_id(categorie_id, database)


@router.post('/', status_code=status.HTTP_201_CREATED)
async def create_offre(request: schemas.Offre, database: Session = Depends(db.get_db)):
    categorie = await validator.verify_categorie_exist(request.categorie_id, database)
    if not categorie:
        raise HTTPException(
            status_code=400,
            detail="You have provided invalid category id.",
        )

    offre = await services.create_new_offre(request, database)
    return offre


@router.get('/', response_model=List[schemas.OffreListing])
async def get_all_offre(database: Session = Depends(db.get_db)):
    return await services.get_all_offres(database)