from datetime import datetime
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.orm import relationship
from recherche_emploi.database.db import Base
from . import hashing


class User(Base):
    __tablename__ = "Users"

    id = Column(Integer, primary_key=True, autoincrement=True)
    email = Column(String(255), unique=True)
    mot_de_passe = Column(String(255))
    date_creation_compte = Column(DateTime, default=datetime.now)
    image = Column(String(255), unique=True)

    message = relationship("Message", back_populates="user_info")
    entreprise = relationship("Entreprise", back_populates="user_entreprise")
    candidat = relationship("Candidat", back_populates="user_candidat")

    def __init__(self, name, email, password, *args, **kwargs):
        self.name = name
        self.email = email
        self.password = hashing.get_password_hash(password)

    def check_password(self, password):
        return hashing.verify_password(self.password, password)
    