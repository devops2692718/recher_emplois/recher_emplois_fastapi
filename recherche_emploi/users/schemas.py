from datetime import datetime
from pydantic import BaseModel, constr, validator, EmailStr

from recherche_emploi.database import db
from . import models


class User(BaseModel):
    email: str
    mot_de_passe: str
    date_creation_compte : datetime
    image: str


class DisplayUser(BaseModel):
    id: int
    nom: str
    email: str

    class Config:
        orm_mode = True