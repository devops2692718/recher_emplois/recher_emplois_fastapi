from recherche_emploi.users import router as user_router
from recherche_emploi.offres_categories import router as offre_router
from recherche_emploi.entreprises import router as entreprise_router

from fastapi import APIRouter


api_router = APIRouter()
api_router.include_router(user_router.router)
api_router.include_router(offre_router.router)
api_router.include_router(entreprise_router.router)