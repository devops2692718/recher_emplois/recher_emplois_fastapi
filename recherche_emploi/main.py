from fastapi import APIRouter, FastAPI
from recherche_emploi.core.api import api_router
from pathlib import Path
from fastapi.templating import Jinja2Templates

description = """
recherche_emploi API

## Offres

You will be able to:

* **Create users** 
* **Read read users** 
"""

root_router  = APIRouter()
recherche_emploi  = FastAPI(
    title="recherche_emploi_app_users ",
    description=description,
    version="0.0.1",
    terms_of_service="http://example.com/terms/",
    contact={
        "name": "Devops interview",
        "url": "http://x-force.example.com/contact/",
    },
    license_info={
        "name": "Apache 2.0",
        "url": "https://www.apache.org/licenses/LICENSE-2.0.html",
    },
)

@root_router.get("/")
async def root():
    return {"message": "Hello World"}

recherche_emploi.include_router(root_router)
recherche_emploi.include_router(api_router)

# 5
if __name__ == "__main__":
    # Use this for debugging purposes only
    import uvicorn
    uvicorn.run(recherche_emploi, host="0.0.0.0", port=8002, log_level="debug")
