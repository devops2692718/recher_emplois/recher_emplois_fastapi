from datetime import datetime
from typing import Optional

from pydantic import BaseModel, constr


class Entreprise(BaseModel):
    nom: str
    num_reg_com: str
    adresse: str
    Secteur_activite: str
    description: str
    annee_creation: datetime
    site_web: str
    telephone: str
    siege_social: str

class EntrepriseBase(BaseModel):
    id: Optional[int]
    nom: str
    num_reg_com: str
    adresse: str
    Secteur_activite: str
    description: str
    annee_creation: datetime
    site_web: str
    telephone: str
    siege_social: str

    class Config:
        orm_mode = True


class ListEntreprise(EntrepriseBase):
    id: int
    nom: str
    description: str

    class Config:
        orm_mode = True



