from typing import Optional

from sqlalchemy.orm import Session

from .models import Entreprise


async def verify_entreprise_exist(entreprise_id: int, db_session: Session) -> Optional[Entreprise]:
    return db_session.query(Entreprise).filter(Entreprise.id == entreprise_id).first()