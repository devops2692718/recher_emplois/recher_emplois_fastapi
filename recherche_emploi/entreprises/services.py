from typing import List

from fastapi import HTTPException, status

from . import models


async def create_new_entreprise(request, database) -> models.Entreprise:
    new_entreprise = models.Entreprise(name=request.name)
    database.add(new_entreprise)
    database.commit()
    database.refresh(new_entreprise)
    return new_entreprise


async def get_all_entreprises(database) -> List[models.Entreprise]:
    entreprises = database.query(models.Entreprise).all()
    return entreprises


async def get_entreprise_by_id(entreprise_id, database) -> models.Entreprise:
    entreprise_info = database.query(models.Entreprise).get(entreprise_id)
    if not entreprise_info:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Data Not Found !")
    return entreprise_info


async def delete_entreprise_by_id(entreprise_id, database):
    database.query(models.Entreprise).filter(models.Entreprise.id == entreprise_id).delete()
    database.commit()

