from typing import List

from fastapi import APIRouter, Depends, status, Response, HTTPException
from sqlalchemy.orm import Session

from recherche_emploi.database import db
from . import schemas
from . import services
from . import validator

router = APIRouter(
    tags=['Entreprise'],
    prefix='/entreprise'
)


@router.post('/entreprise', status_code=status.HTTP_201_CREATED)
async def create_entreprise(request: schemas.Entreprise, database: Session = Depends(db.get_db)):
    new_entreprise = await services.create_new_entreprise(request, database)
    return new_entreprise


@router.get('/entreprise', response_model=List[schemas.ListEntreprise])
async def get_all_entreprises(database: Session = Depends(db.get_db)):
    return await services.get_all_entreprises(database)


@router.get('/entreprise/{entreprise_id}', response_model=schemas.ListEntreprise)
async def get_entreprise_by_id(entreprise_id: int, database: Session = Depends(db.get_db)):
    return await services.get_entreprise_by_id(entreprise_id, database)


@router.delete('/entreprise/{entreprise_id}', status_code=status.HTTP_204_NO_CONTENT, response_class=Response)
async def delete_entreprise_by_id(entreprise_id: int, database: Session = Depends(db.get_db)):
    return await services.delete_entreprise_by_id(entreprise_id, database)

