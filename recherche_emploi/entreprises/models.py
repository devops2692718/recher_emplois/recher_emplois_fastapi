from datetime import datetime
from sqlalchemy import Column, Integer, String, Float, ForeignKey, Text, DateTime
from sqlalchemy.orm import relationship

from recherche_emploi.database.db import Base


class Entreprise(Base):
    __tablename__ = "Entreprise"

    id = Column(Integer, primary_key=True, autoincrement=True)
    nom = Column(String(50))
    num_reg_com = Column(String(50))
    adresse = Column(String(50))
    Secteur_activite = Column(String(50))
    description = Column(Text)
    annee_creation = Column(DateTime, default=datetime.now)
    site_web = Column(String(50))
    telephone = Column(String(50))
    siege_social = Column(String(50))
    user_entreprise = relationship("Users", back_populates="entreprise")
    offre = relationship("Offres", back_populates="entrerpise")
    
