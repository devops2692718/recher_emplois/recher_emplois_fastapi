FROM python:3.9

# create the devops user
RUN addgroup --system devops && adduser --system --group devops

WORKDIR  /recherche_emploi/

# https://docs.python.org/3/using/cmdline.html#envvar-PYTHONDONTWRITEBYTECODE
# Prevents Python from writing .pyc files to disk
ENV PYTHONDONTWRITEBYTECODE 1

# ensures that the python output is sent straight to terminal (e.g. your container log)
# without being first buffered and that you can see the output of your application (e.g. django logs)
# in real time. Equivalent to python -u: https://docs.python.org/3/using/cmdline.html#cmdoption-u
ENV PYTHONUNBUFFERED 1
ENV ENVIRONMENT prod
ENV TESTING 0

# Install Poetry
RUN curl -sSL https://install.python-poetry.org | POETRY_HOME=/opt/poetry python && \
    cd /usr/local/bin && \
    ln -s /opt/poetry/bin/poetry && \
    poetry config virtualenvs.create false


# Copy poetry.lock* in case it doesn't exist in the repo
COPY pyproject.toml poetry.lock*  /recherche_emploi/


# Allow installing dev dependencies to run tests
ARG INSTALL_DEV=false

RUN bash -c "if [ $INSTALL_DEV == 'true' ] ; then poetry install --no-root ; else poetry install --no-root --no-dev ; fi"

COPY . /recherche_emploi
RUN chmod +x run.sh

ENV PYTHONPATH=/recherche_emploi
EXPOSE 8002

# chown all the files to the devops user
RUN chown -R devops:devops $HOME

# switch uer to devops
USER devops

#Lancer Univcorn
CMD ["./run.sh"]